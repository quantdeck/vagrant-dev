
require 'json'
#OS detection
module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
   (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end

module Helpers

  def Helpers.copy_private (folders, config)
    config.vm.synced_folder '~', '/host_home'

    folders.each do |f|   
      config.vm.provision :shell, 
        :inline => "cp /host_home/#{f} /home/vagrant/#{f} -a"
      config.vm.provision :shell, 
        :inline => "chmod -R 700 /home/vagrant/#{f}  "      
    end
    
  end

end

# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  config.vm.host_name = 'plover.local'

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = 'chef/debian-7.4'

  #Enable SSH forwarding for bitbucket private repos etc
  config.ssh.forward_agent = true

  #Enable chef
  config.omnibus.chef_version = :latest

  #Enable berkshelf  
  config.berkshelf.enabled = true

  #update
  config.vm.provision :shell, :inline => "/usr/bin/apt-get update --fix-missing"  

  #Share dev folder
  if OS.windows?
    config.vm.synced_folder '~/Documents/Development', '/quantdeck'
  else
    config.vm.synced_folder '~/Development', '/quantdeck'
  end

  #disable the  vagrant folder
  config.vm.synced_folder '.', '/vagrant', disabled: true  


  #copy private files from home folders  
  Helpers.copy_private ['.aws', '.quantdeck'], config
  
  #cookbooks and rvm
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = 'cookbooks'
    chef.add_recipe 'apt'
    chef.add_recipe 'git'
    chef.add_recipe 'nodejs'    
    chef.add_recipe 'awscli'    
    chef.add_recipe "rvm::system"
    chef.add_recipe "rvm::vagrant"  
    chef.add_recipe "postgresql::client"
    chef.add_recipe "postgresql::ruby"

     chef.json = {
      'rvm' => {
         'rubies' => ["2.0.0-p353", "2.1.2"],
         'default_ruby' => "2.1.2",
         :vagrant => { :system_chef_solo => "/opt/chef/bin/chef-solo" }
      }
    }    
  end

  #create bundles dir
  config.vm.provision :shell, 
        :inline => 'mkdir -p /home/vagrant/.bundles'
#copy environment variables across
  $script = <<SCRIPT
  if [ -f /host_home/.quantdeck/environment.sh ]
    then
    cp /host_home/.quantdeck/environment.sh /etc/profile.d/environment.sh
    chmod +x /etc/profile.d/environment.sh
    fi
SCRIPT

  #add external shell script
  config.vm.provision :shell, 
        :inline => $script
 end